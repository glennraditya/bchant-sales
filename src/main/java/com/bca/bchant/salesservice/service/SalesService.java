package com.bca.bchant.salesservice.service;

import java.util.List;

import com.bca.bchant.salesservice.model.ErrorSchema;
import com.bca.bchant.salesservice.model.ListSales;
import com.bca.bchant.salesservice.model.Sales;

public interface SalesService {
	ErrorSchema setSales (String trxId, String customerId);
	List<Sales> setSalesTemp (List<Sales> listSales, String merchantId, String createdBy);
	ListSales getSalesDetail (String trxId);
	List<Sales> getSalesStatus (String trxId);
	ErrorSchema	updateSalesStatus (String trxId, String customerId);
}
