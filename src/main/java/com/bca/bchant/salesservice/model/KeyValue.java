package com.bca.bchant.salesservice.model;

public class KeyValue {
    private String Key;
    private String Value;

    public KeyValue(){
    }
    
    public KeyValue(String Key, String Value) {
        this.Key = Key;
        this.Value = Value;
    }
    
    public void setKey(String Key) {
        this.Key = Key;
    }

    public String getKey() {
        return Key;
    }

    public void setValue(String Value) {
        this.Value = Value;
    }

    public String getValue() {
        return Value;
    }

	@Override
	public String toString() {
		return "KeyValue [Key=" + Key + ", Value=" + Value + "]";
	}
    
    
}
