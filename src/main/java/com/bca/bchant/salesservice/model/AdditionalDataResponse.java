package com.bca.bchant.salesservice.model;

import java.util.ArrayList;
import java.util.List;

public class AdditionalDataResponse {
	private List<KeyValue> Data = new ArrayList<KeyValue>();
	
	public List<KeyValue> getData() {
		return Data;
	}

	public void setData(List<KeyValue> data) {
		Data = data;
	}
}
