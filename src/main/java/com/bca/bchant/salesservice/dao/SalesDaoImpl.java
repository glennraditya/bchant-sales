package com.bca.bchant.salesservice.dao;

import java.security.MessageDigest;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.bca.bchant.salesservice.dbconn.getDbConnection;
import com.bca.bchant.salesservice.model.AccessTokenResponse;
import com.bca.bchant.salesservice.model.ErrorSchema;
import com.bca.bchant.salesservice.model.FirebaseResponse;
import com.bca.bchant.salesservice.model.IDSResponse;
import com.bca.bchant.salesservice.model.ListSales;
import com.bca.bchant.salesservice.model.Sales;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import oracle.jdbc.OracleTypes;

@Repository
public class SalesDaoImpl implements SalesDao{

	private static final Logger LOGGER = LoggerFactory.getLogger(SalesDaoImpl.class);

	Connection conn = null;
	CallableStatement callableStatement = null;
	PreparedStatement pSt = null;
	PreparedStatement pSt2 = null;
	PreparedStatement pSt3 = null;
	PreparedStatement pSt4 = null;
	ResultSet rs = null;
	ResultSet rs2 = null;
	ResultSet rs3 = null;
	ResultSet rs4 = null;
	
	public static String hMacRequestString(String requestString, String key, String algorithm) throws Exception {
        SecretKeySpec secretKey = new SecretKeySpec(key.getBytes("UTF-8"), algorithm);
        
        Mac hmac = Mac.getInstance(algorithm);
        hmac.init(secretKey);
        byte[] hmacAsBytes = hmac.doFinal(requestString.getBytes("UTF-8"));
        
        return new String(Hex.encodeHex(hmacAsBytes)).toLowerCase();
    }
	
	@Override
	public ErrorSchema setSales(String trxId, String customerId) {
		ErrorSchema errorSchema = new ErrorSchema();
		
		Date dateNow = Calendar.getInstance().getTime();
		StringBuilder sb = new StringBuilder();
		
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").format(dateNow);
		String timeStampTransfer = new SimpleDateFormat("ddMMyyyy").format(dateNow);
		String clientId = "7AC5C6738ED81CFCE05400144FFA3B5D";
		String apiKey = "58867dad-3387-44ef-a84b-30b342b1f8f9";
		String accessToken = "";
		String authorization = "";
		String totalHarga = "";
		String merchantId = "";
		String rekMerchant = "";
		String rekCustomer = "";
		String bodyBalanceInquiry = "";
		String bodyTransfer = "";
		
		String balanceCustomer = "";
		
		RestTemplate rest = new RestTemplate();
		String url = "https://devapi.klikbca.com/api/oauth/token";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.add("Authorization", "Basic Nzg2MzNkNDgtYmZiYi00NDMzLTlhOTQtYWY5NmU3MmE3Nzg0OjNjODA1MTAzLTVmZWItNDBjOC04MzA3LWVlZDVkYmM0OGYxYQ==");
		
		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("grant_type", "client_credentials");
		
		HttpEntity<MultiValueMap<String, String>> headerG = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		ResponseEntity<AccessTokenResponse> responseAccessToken = rest.postForEntity(url, headerG , AccessTokenResponse.class);
		
		LOGGER.info("accesstoken : " + responseAccessToken.getBody().getAccess_token());
		accessToken = responseAccessToken.getBody().getAccess_token();
		authorization = "Bearer " + accessToken;
		
		String query = "SELECT SUM(total) AS jumlah_total, merchantid "
				+ "FROM sales WHERE trxid = ? GROUP BY merchantid ";
		
		String query2 = "SELECT rekmerchant "
				+ "FROM merchant WHERE merchantid = ? ";
		
		String query3 = "SELECT rekcustomer "
				+ "FROM customer WHERE customerid = ? ";
		
		String query4 = "SELECT s.productid AS productid, p.qty AS stock, s.qty AS qtyjual "
				+ "FROM sales s "
				+ "JOIN product p ON p.productid = s.productid "
				+ "WHERE trxid = ? ";
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + query + " ...");
			
			pSt = conn.prepareStatement(query);
			LOGGER.info("Prepared Statement dengan isi " + query + " berhasil dijalanakan ...");
			
			pSt.setString(1, trxId);
			
			rs = pSt.executeQuery();
			
			while(rs.next()){
				totalHarga = rs.getString("jumlah_total");
				merchantId = rs.getString("merchantid");
			}
			
			LOGGER.info("Berhasil mendapatkan data jumlah total transaksi : " + totalHarga);
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + query2 + " ...");
			
			pSt2 = conn.prepareStatement(query2);
			LOGGER.info("Prepared Statement dengan isi " + query2 + " berhasil dijalanakan ...");
			
			pSt2.setString(1, merchantId);
			
			rs2 = pSt2.executeQuery();
			
			while(rs2.next()){
				rekMerchant = rs2.getString("rekmerchant");
			}
			
			LOGGER.info("Berhasil mendapatkan data rekening merchant : " + rekMerchant);
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + query3 + " ...");
			
			pSt3 = conn.prepareStatement(query3);
			LOGGER.info("Prepared Statement dengan isi " + query3 + " berhasil dijalanakan ...");
			
			pSt3.setString(1, customerId);
			
			rs3 = pSt3.executeQuery();
			
			while(rs3.next()){
				rekCustomer = rs3.getString("rekcustomer");
			}
			
			LOGGER.info("Berhasil mendapatkan data rekening customer : " + rekCustomer);
			
			bodyBalanceInquiry = "{\"TrxCd\":\"CBSME103\",\"MainData\":[{\"Key\":\"accountNumber\",\"Value\":\"" + rekCustomer + "\"},{\"Key\":\"userId\",\"Value\":\"SME001\"}],\"AdditionalData\":[]}";
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			digest.update(bodyBalanceInquiry.getBytes("UTF-8"));

			String hashedRequestBody = new String(Hex.encodeHex(digest.digest()));
			
			String sb2 = sb.append("POST").append(":").append("https://devapi.klikbca.com/BChant/BalanceInquiry").append(":").append(accessToken).append(":").append(hashedRequestBody).append(":").append(timeStamp).toString();

			String signature = hMacRequestString(sb2, apiKey, "HmacSHA256");
			
			url = "https://devapi.klikbca.com/BChant/BalanceInquiry";
			headers = new HttpHeaders();
			headers.add("Content-Type", "application/json");
			headers.add("Authorization", authorization);
			headers.add("X-BCA-ClientID", clientId);
			headers.add("X-BCA-Key", apiKey);
			headers.add("X-BCA-Signature", signature);
			headers.add("X-BCA-Timestamp", timeStamp);
			
			rest = new RestTemplate();
			HttpEntity<String> entity = new HttpEntity<String>(bodyBalanceInquiry, headers);
			String response = rest.postForObject(url, entity,String.class);
			LOGGER.info("response body Balance Inquiry -> " + response);
			
			Gson gson = new Gson(); // Or use new GsonBuilder().create();
			IDSResponse responseBalanceInquiry = gson.fromJson(response, IDSResponse.class);
			
			for(int i = 0; i < responseBalanceInquiry.getMainData().size(); i++){
				if(responseBalanceInquiry.getMainData().get(i).getKey().equals("availableBalance")){
					balanceCustomer = responseBalanceInquiry.getMainData().get(i).getValue();
					LOGGER.info("Berhasil Inquiry Balance Customer");
				}
				else if(responseBalanceInquiry.getMainData().get(i).getKey().equals("errorMessage")){
					throw new Exception(responseBalanceInquiry.getMainData().get(i).getValue());
				}
			}

			balanceCustomer = balanceCustomer.replaceFirst("^0+(?!$)", "");
			balanceCustomer = balanceCustomer.substring(0,balanceCustomer.length()-1);
			LOGGER.info("balanceCustomer : " + balanceCustomer);
			LOGGER.info("TotalHarga : " + Long.parseLong(totalHarga) * 100);
			
			if(Long.parseLong(balanceCustomer) < (Long.parseLong(totalHarga) * 100)){
				throw new Exception("Balance Customer tidak cukup");
			}
			else{
				bodyTransfer = "{\"TrxCd\":\"CBSME404\"," + 
								"\"MainData\":" + 
								"[{\"Key\":\"cicsTxn\",\"Value\":\"SME4\"}," + 
								 "{\"Key\":\"wsid\",\"Value\":\"95031\"}," + 
								"{\"Key\":\"txnCode\",\"Value\":\"800\"}," + 
								"{\"Key\":\"txnRefNo\",\"Value\":\"00286406512\"}," + 
								"{\"Key\":\"acctNoFrom\",\"Value\":\""+ rekCustomer +"\"}," + 
								"{\"Key\":\"acctNoTo\",\"Value\":\""+ rekMerchant +"\"}," + 
								"{\"Key\":\"logType\",\"Value\":\"L\"}," + 
								"{\"Key\":\"amtExchange\",\"Value\":\""+ totalHarga +"00\"}," + 
								"{\"Key\":\"amtInput\",\"Value\":\""+ totalHarga +"00\"}," + 
								"{\"Key\":\"amtKurs\",\"Value\":\"100\"}," + 
								"{\"Key\":\"amtCharges\",\"Value\":\"0\"}," + 
								"{\"Key\":\"currCode\",\"Value\":\"IDR\"}," + 
								"{\"Key\":\"busnDate\",\"Value\":\"07072016\"}," + 
								"{\"Key\":\"servBranch\",\"Value\":\"0061\"}," + 
								"{\"Key\":\"desc1\",\"Value\":\"\"}," + 
								"{\"Key\":\"desc2\",\"Value\":\"\"}," + 
								"{\"Key\":\"desc3\",\"Value\":\"\"}," + 
								"{\"Key\":\"desc4\",\"Value\":\"\"}," + 
								"{\"Key\":\"desc5\",\"Value\":\"\"}]," + 
							"\"AdditionalData\":[]}";
				digest.reset();
				digest = MessageDigest.getInstance("SHA-256");
				digest.update(bodyTransfer.getBytes("UTF-8"));

				hashedRequestBody = new String(Hex.encodeHex(digest.digest()));
				
				sb = new StringBuilder();
				sb2 = sb.append("POST").append(":").append("https://devapi.klikbca.com/BChant/Transfer").append(":").append(accessToken).append(":").append(hashedRequestBody).append(":").append(timeStamp).toString();

				signature = hMacRequestString(sb2, apiKey, "HmacSHA256");
				
				url = "https://devapi.klikbca.com/BChant/Transfer";
				headers = new HttpHeaders();
				headers.add("Content-Type", "application/json");
				headers.add("Authorization", authorization);
				headers.add("X-BCA-ClientID", clientId);
				headers.add("X-BCA-Key", apiKey);
				headers.add("X-BCA-Signature", signature);
				headers.add("X-BCA-Timestamp", timeStamp);
				
				rest = new RestTemplate();
				entity = new HttpEntity<String>(bodyTransfer, headers);
				response = rest.postForObject(url, entity,String.class);
				LOGGER.info("response body Transfer -> " + response);
				
				gson = new Gson(); // Or use new GsonBuilder().create();
				IDSResponse responseTransfer = gson.fromJson(response, IDSResponse.class);
				
				for(int i = 0; i < responseTransfer.getMainData().size(); i++){
					if(responseTransfer.getMainData().get(i).getKey().equals("status")){
						if(responseTransfer.getMainData().get(i).getValue().equals("TRAN SUCCESSFUL")){
							//errorSchema.setErrorCode("ESB-00-000");
							//errorSchema.setErrorMessage(responseTransfer.getMainData().get(i).getValue());
							LOGGER.info("Berhasil Transfer Dana");
						}
						else{
							throw new Exception(responseTransfer.getMainData().get(i).getValue());
						}
					}
					else if(responseTransfer.getMainData().get(i).getKey().equals("errorMessage")){
						throw new Exception(responseTransfer.getMainData().get(i).getValue());
					}
				}
				
				String spName = "UPDATE_SALES_STATUS";
				String errorMessage = "";
				Integer errorMessageNumb = 0;
				
				//StoredProcedure
				LOGGER.info("Siap menjalankan stored procedure " + spName + " ...");
				
				callableStatement = conn.prepareCall("{call " + spName + "(?,?,?)}");
				LOGGER.info("Stored Procedure dengan nama " + spName + " berhasil dijalanakan ...");
				
				callableStatement.setString(1, customerId);
				callableStatement.setString(2, trxId);
				callableStatement.registerOutParameter(3, OracleTypes.CURSOR);
				LOGGER.info("Menjalankan registerOutParameter untuk Cursor dengan value -> " + OracleTypes.CURSOR + " ...");
				
				callableStatement.executeUpdate();
				LOGGER.info("Mengeksekusi stored procedure dengan nama " + spName + " ...");
		
				rs = (ResultSet) callableStatement.getObject(3);
				while(rs.next()){
					errorMessageNumb = rs.getInt("v_error_msg_numb");
					errorMessage = rs.getString("v_error_msg");
				}
				
				if(errorMessageNumb == 1) {
					//errorSchema.setErrorCode("ESB-00-00");
					//errorSchema.setErrorMessage(errorMessage);
					LOGGER.info("Berhasil melakukan pembayaran dan update status sales dengan id trx " + trxId + " ...");
					
					//PreparedStatement
					LOGGER.info("Siap menjalankan query " + query4 + " ...");
					
					pSt4 = conn.prepareStatement(query4);
					LOGGER.info("Prepared Statement dengan isi " + query4 + " berhasil dijalanakan ...");
					
					pSt4.setString(1, trxId);
					
					rs4 = pSt4.executeQuery();
					
					HashMap<String, Integer> hashStock = new HashMap<String, Integer>();
					String productId = "";
					Integer stock;
					Integer qtyJual;
					while(rs4.next()){
						productId = rs4.getString("productid");
						if(!rs4.getString("stock").equals("-")){
							stock = Integer.parseInt(rs4.getString("stock"));
							qtyJual = Integer.parseInt(rs4.getString("qtyjual"));
							stock = stock - qtyJual;
							
							spName = "UPDATE_SALES_STOCK";
							errorMessage = "";
							errorMessageNumb = 0;
							
							//StoredProcedure
							LOGGER.info("Siap menjalankan stored procedure " + spName + " ...");
							
							callableStatement = conn.prepareCall("{call " + spName + "(?,?,?)}");
							LOGGER.info("Stored Procedure dengan nama " + spName + " berhasil dijalanakan ...");
							
							callableStatement.setString(1, productId);
							callableStatement.setString(2, stock.toString());
							callableStatement.registerOutParameter(3, OracleTypes.CURSOR);
							LOGGER.info("Menjalankan registerOutParameter untuk Cursor dengan value -> " + OracleTypes.CURSOR + " ...");
							
							callableStatement.executeUpdate();
							LOGGER.info("Mengeksekusi stored procedure dengan nama " + spName + " ...");
					
							rs = (ResultSet) callableStatement.getObject(3);
							while(rs.next()){
								errorMessageNumb = rs.getInt("v_error_msg_numb");
								errorMessage = rs.getString("v_error_msg");
							}
							
							if(errorMessageNumb == 1){
								//errorSchema.setErrorCode("ESB-00-000");
								//errorSchema.setErrorMessage("Berhasil update stock product " + productId + " menjadi " + stock.toString());
								LOGGER.error("Berhasil update stock product " + productId + " menjadi " + stock.toString());
							}
							else{
								throw new Exception("Gagal melakukan update stock tetapi pembayaran berhasil dilakukan");
							}
							
						}
					}
					
					LOGGER.info("Berhasil mendapatkan data jumlah total transaksi : " + totalHarga);
					
					rest = new RestTemplate();
					url = "http://10.1.124.199:8081/send";
					headers = new HttpHeaders();
					headers.add("Authorization", "key=AAAACANRlHc:APA91bHkvhpEYU9CQ08XgqyMactKxfRscMnYSmKXPqIl9VT0EJKaM3JMzmHxMPPqlE1BkU9mqR3C1c8Qe2auZOv5c0wxi8Z69YWqVE6ioOMGdKXG8Cp4lyF0pq26kWY4t_KcpsPBFeep");
					headers.add("To", "dYc3rh_QphY:APA91bGPfM8EUGLISjRAIitcG_uEVPbw0dr8FozeKB2GW8LtAu_rKIj9iEKRkou3z7B3pSFk2IQBVevNh-pi3mFfiLAIqdlRbGenBICu_pNXkn47bRkv-ZfvS31W_aBGb79GR3-Cr5FS");
					headers.add("TrxID", trxId);
					headerG = new HttpEntity<>(headers);
					ResponseEntity<FirebaseResponse> responseFirebase = rest.exchange(url, HttpMethod.GET, headerG, FirebaseResponse.class);
					
					if(responseFirebase.getBody().getSuccess() == 1){
						errorSchema.setErrorCode("ESB-00-000");
						errorSchema.setErrorMessage("Berhasil melakukan pembayaran dan update status sales dengan id trx " + trxId + " dan notifikasi Firebase sudah dimunculkan");
						LOGGER.info("Berhasil melakukan pembayaran dan update status sales dengan id trx " + trxId + " dan notifikasi Firebase sudah dimunculkan");
					}
					else{
						throw new Exception("Berhasil melakukan pembayaran dan update status sales dengan id trx " + trxId + " tetapi Notifikasi Firebase gagal dimunculkan");
					}
					
				}
				else {
					throw new Exception("Gagal melakukan update status sales");
				}
				
			}
		} catch(Exception e) {
			
			errorSchema.setErrorCode("ESB-99-999");
			
			
			rest = new RestTemplate();
			url = "http://10.1.124.199:8081/sendFailed";
			headers = new HttpHeaders();
			headers.add("Authorization", "key=AAAACANRlHc:APA91bHkvhpEYU9CQ08XgqyMactKxfRscMnYSmKXPqIl9VT0EJKaM3JMzmHxMPPqlE1BkU9mqR3C1c8Qe2auZOv5c0wxi8Z69YWqVE6ioOMGdKXG8Cp4lyF0pq26kWY4t_KcpsPBFeep");
			headers.add("To", "dYc3rh_QphY:APA91bGPfM8EUGLISjRAIitcG_uEVPbw0dr8FozeKB2GW8LtAu_rKIj9iEKRkou3z7B3pSFk2IQBVevNh-pi3mFfiLAIqdlRbGenBICu_pNXkn47bRkv-ZfvS31W_aBGb79GR3-Cr5FS");
			headers.add("TrxID", trxId);
			headerG = new HttpEntity<>(headers);
			ResponseEntity<FirebaseResponse> responseFirebase = rest.exchange(url, HttpMethod.GET, headerG, FirebaseResponse.class);
			
			if(responseFirebase.getBody().getSuccess() == 1){
				errorSchema.setErrorMessage(e.getMessage() + " dan berhasil memunculkan notifikasi Firebase gagal transaksi");
				LOGGER.info("Berhasil memunculkan notifikasi Firebase gagal transaksi");
			}
			else{
				errorSchema.setErrorMessage(e.getMessage() + " tetapi tidak berhasil memunculkan notifikasi Firebase gagal transaksi");
				LOGGER.info("Tidak berhasil memunculkan notifikasi Firebase gagal transaksi");
			}
			
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}
				if (rs2 != null) {
					rs2.close();
				}
				if (rs3 != null) {
					rs3.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
				if (pSt2 != null) {
					pSt2.close();
				}
				if (pSt3 != null) {
					pSt3.close();
				}
				
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		
		return errorSchema;
	}

	public List<Sales> setSalesTemp(List<Sales> listSales, String merchantId, String createdBy){
		List<Sales> listSalesOutput = new ArrayList<Sales>();
		String errorMessage = "";
		Integer errorMessageNumb = 0;
		String trxId = "";
		boolean flagError = false;
		String spName = "INSERT_SALES_TEMP";
		String timeStamp2 = "";
		Date dateNow = Calendar.getInstance().getTime();
		
		String timeStamp = new SimpleDateFormat("dd-MMM-yy hh:mm:ss.SSS a").format(dateNow);
		LOGGER.info("TimeStamp : " + timeStamp);
		
		timeStamp2 = new SimpleDateFormat("ddMMyyHHmmssSSS").format(dateNow);
		
		trxId = merchantId + timeStamp2;
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			for(int i = 0; i < listSales.size(); i++){
				//StoredProcedure
				callableStatement = conn.prepareCall("{call " + spName + "(?,?,?,?,?,?,?,?,?)}");
				
				callableStatement.setString(1, trxId);
				callableStatement.setString(2, merchantId);
				callableStatement.setString(3, listSales.get(i).getTotal());
				callableStatement.setString(4, listSales.get(i).getProductId());
				callableStatement.setString(5, listSales.get(i).getQty());
				callableStatement.setString(6, listSales.get(i).getHarga());
				callableStatement.setString(7, timeStamp);
				callableStatement.setString(8, createdBy);
				callableStatement.registerOutParameter(9, OracleTypes.CURSOR);
				
				callableStatement.executeUpdate();
		
				rs = (ResultSet) callableStatement.getObject(9);
				while(rs.next()){
					errorMessageNumb = rs.getInt("v_error_msg_numb");
					errorMessage = rs.getString("v_error_msg");
					LOGGER.info("ErrorMessage : " + errorMessage);
					if(errorMessageNumb != 1) flagError = true;
				}
			}
			
			
			
			Sales outputSales = new Sales();
			
			if(flagError == true) {
				outputSales.setTrxId(errorMessageNumb.toString());
				listSalesOutput.add(outputSales);
			}
			else {
				outputSales.setTrxId(trxId);
				listSalesOutput.add(outputSales);
			}
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listSalesOutput;
	}

	public ListSales getSalesDetail (String trxId){
		ListSales listSalesOutputAll = new ListSales();
		List<Sales> listSalesOutput = new ArrayList<Sales>();
		String grandTotal = "";
		
		String query = "SELECT SUM(total) AS grandtotal "
				+ "FROM sales WHERE trxid = ?";
		
		String query2 = "SELECT s.trxid AS trxid, s.merchantid AS merchantid, m.merchantname AS merchantname, s.total AS total, "
				+ "s.productid AS productid, p.nama AS productname, s.qty AS qty, s.hargast AS hargast, s.tgl AS tgl, s.status AS status "
				+ "FROM sales s "
				+ "JOIN merchant m ON m.merchantid = s.merchantid "
				+ "JOIN product p ON p.productid = s.productid "
				+ "WHERE s.trxid = ? "
				+ "ORDER BY s.productid ASC";
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + query + " ...");
			
			pSt = conn.prepareStatement(query);
			LOGGER.info("Prepared Statement dengan isi " + query + " berhasil dijalanakan ...");
			
			pSt.setString(1, trxId);
			
			rs = pSt.executeQuery();
	
			while(rs.next()){
				grandTotal = rs.getString("grandtotal");
				listSalesOutputAll.setGrandTotal(grandTotal);
			}
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + query2 + " ...");
			
			pSt2 = conn.prepareStatement(query2);
			LOGGER.info("Prepared Statement dengan isi " + query2 + " berhasil dijalanakan ...");
			
			pSt2.setString(1, trxId);
			
			rs2 = pSt2.executeQuery();
	
			while(rs2.next()){
				Sales sales = new Sales();
				
				sales.setTrxId(rs2.getString("trxid"));
				sales.setMerchantId(rs2.getString("merchantid"));
				sales.setMerchantName(rs2.getString("merchantname"));
				sales.setTotal(rs2.getString("total"));
				sales.setProductId(rs2.getString("productid"));
				sales.setProductName(rs2.getString("productname"));
				sales.setQty(rs2.getString("qty"));
				sales.setHarga(rs2.getString("hargast"));
				sales.setTgl(rs2.getString("tgl"));
				sales.setStatus(rs2.getString("status"));
				listSalesOutput.add(sales);
			}
			
			listSalesOutputAll.setListSales(listSalesOutput);
			LOGGER.info("Berhasil mendapatkan data sales");
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listSalesOutputAll;
	}
	
	public List<Sales> getSalesStatus (String trxId){
		List<Sales> listSalesOutput = new ArrayList<Sales>();
		
		String query = "SELECT DISTINCT(status) "
				+ "FROM sales WHERE trxid = ? ";
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + query + " ...");
			
			pSt = conn.prepareStatement(query);
			LOGGER.info("Prepared Statement dengan isi " + query + " berhasil dijalanakan ...");
			
			pSt.setString(1, trxId);
			
			rs = pSt.executeQuery();
	
			while(rs.next()){
				Sales sales = new Sales();
				
				sales.setStatus(rs.getString("status"));
				listSalesOutput.add(sales);
			}
			LOGGER.info("Berhasil mendapatkan data sales");
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listSalesOutput;
	}

	public ErrorSchema updateSalesStatus (String trxId, String customerId){
		ErrorSchema errorSchema = new ErrorSchema();
		String errorMessage = "";
		Integer errorMessageNumb = 0;
		String spName = "UPDATE_SALES_STATUS";
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//StoredProcedure
			LOGGER.info("Siap menjalankan stored procedure " + spName + " ...");
			
			callableStatement = conn.prepareCall("{call " + spName + "(?,?,?)}");
			LOGGER.info("Stored Procedure dengan nama " + spName + " berhasil dijalanakan ...");
			
			callableStatement.setString(1, customerId);
			callableStatement.setString(2, trxId);
			callableStatement.registerOutParameter(3, OracleTypes.CURSOR);
			LOGGER.info("Menjalankan registerOutParameter untuk Cursor dengan value -> " + OracleTypes.CURSOR + " ...");
			
			callableStatement.executeUpdate();
			LOGGER.info("Mengeksekusi stored procedure dengan nama " + spName + " ...");
	
			rs = (ResultSet) callableStatement.getObject(3);
			while(rs.next()){
				errorMessageNumb = rs.getInt("v_error_msg_numb");
				errorMessage = rs.getString("v_error_msg");
			}
			
			if(errorMessageNumb == 1) {
				errorSchema.setErrorCode("ESB-00-00");
				errorSchema.setErrorMessage(errorMessage);
				LOGGER.info("Berhasil melakukan update status sales dengan id trx " + trxId + " ...");
			}
			else {
				errorSchema.setErrorCode("ESB-99-999");
				errorSchema.setErrorMessage(errorMessage);
				LOGGER.error("Gagal melakukan update ...");
			}
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return errorSchema;
	}
}
